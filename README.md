# Aplicação para novos desenvolvedores FrontEnd - Ignição digital

Olá, se você chegou até aqui, provavelmente se interessou por uma de nossas vagas em aberto para desenvolvedores FrontEnd.


# Como fazer sua aplicação
Para se candidatar é simples:

+ Faça um fork deste repositório;

+ Crie um branch com o seu nome;

+ Para o teste prático, siga as instruções em src/index.html;

+ Envie um Pull Request utlizando o branch que você criou através do bitbucket;

Boa sorte!